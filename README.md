# CS725-SFTP-RFC913

This code implements the server and client class using the TCP protocol for a Simple File Transfer Protocol (SFTP) as specified in RFC-913.

+ Last updated: 21/08/18
+ Language: Java
+ Author: William Chao
+ E-mail: <wcha609@aucklanduni.ac.nz>

### Table of Contents
- [CS725-SFTP-RFC913](#cs725-sftp-rfc913)
        - [Table of Contents](#table-of-contents)
    - [Dependencies](#dependencies)
    - [Features (+deviations)](#features-deviations)
    - [How to build and execute](#how-to-build-and-execute)
    - [Usage](#usage)
        - [Starting the Server](#starting-the-server)
        - [Starting the Client](#starting-the-client)
        - [Files](#files)
        - [Adding additional users or accounts](#adding-additional-users-or-accounts)
    - [Notes](#notes)
    - [Commands](#commands)
        - [USER `<user_id>`](#user-userid)
        - [ACCT `<account>`](#acct-account)
        - [PASS `<password>`](#pass-password)
        - [TYPE `<A|B|C>`](#type-abc)
        - [LIST `<F|V> <directory (opt)>`](#list-fv-directory-opt)
        - [CDIR `<directory>`](#cdir-directory)
        - [KILL `<file_name>`](#kill-filename)
        - [NAME `<old_file_name>`](#name-oldfilename)
        - [RETR `<file_name>`](#retr-filename)
        - [STOR `<new|old|app> <file_name>`](#stor-newoldapp-filename)
        - [DONE](#done)
    - [Future Work](#future-work)
    - [Authors thoughts about specification](#authors-thoughts-about-specification)

## Dependencies

+ [Java 8+](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
  
## Features (+deviations)

+ Multi-threaded server so multiple clients can connect at once. This means it never sends an `-Out to lunch` message.
+ Exception handling
+ More useful error messages
+ Automatically generates directories for client or users and their accounts (if they don't exist)
+ Has a server startup message
+ Rather than replying with `+Saved file` it replies with `+Saved <file-name>`
+ Dynamically shows percentage received for receiving file to client or sending file to server. (Shown when transferring a large file ~1GB) This indicates to the user that the server or client hasn't froze and is actually still working, rather than showing nothing and waiting for control to be returned to the user.
+ Has file received confirmations on both client and server
+ Each account must have a user. The order that needs to be sent to login is USER > ACCT > PASS. It makes more sense to do it this way because the server needs to verify who you are (user) then know which account you're connecting to, and check whether the password you wrote is correct. Having the server say password correct, send account is the same as giving away a password.
+ Switch accounts or users by specifying the user first then proceed to login
+ Changed a few replies to make a little more sense, for example `!Already logged in` rather than `!Account valid, logged-in` as it may confuse the user when they type a random account and says it is valid.
+ Types B & C are the same since most computers use multiples of 8-bits now
+ `CDIR` allows going back a directory using `..` as it is easier to type rather than the full path
+ `CDIR` allows navigating directly to a folder using `CDIR /path/to/folder` if the path is known
+ `CDIR` only works after logging in as you should not be fiddling around with the server's current directory when the client is unverified.
+ `KILL` is able to delete a directory and all contents inside it including subdirectories
+ Commands are case insensitive

## How to build and execute

1. Execute compile.bat. 
    1. Alternatively, open terminal in `./src/SFTP/server` and use the command `javac *.java` to build the server
    2. Similarly, open terminal in `./src/SFTP/client` and use the command `javac *.java` to build the client 
2. Open terminal in the root directory `./` and use the command `java -cp src/ SFTP.server.TCPMultiServer "6789"` where 6789 can be replaced with any port number
3. Open another terminal in the root directory `./` and use the command `java -cp src/ SFTP.client.TCPClient "localhost" "6789"` where localhost and 6789 and be replaced with the IP address of the server and port number of the server.


## Usage

### Starting the Server

1. Open the terminal on the root directory and use the following command `java -jar TCPServer.jar "6789"` where "6789" can be replaced with any other port number.
2. On the server, you should be greeted with `+Server started on port 6789` or whichever port you have specified.

### Starting the Client

1. Open the terminal on the root directory and use the following command `java -jar TCPClient.jar "localhost" "6789"` where localhost can be replaced with the IP address of the server (if running client on another computer) and 6789 can be replaced with the port specified in the server.
2. On the client, you should be greeted with either `+127.0.0.1 SFTP Service` or `+localhost SFTP Service` or the host name/IP address of your client.

### Files

+ Client files are located in the folder `./client_files` which should be in the same folder of TCPClient.jar
  + This folder should be automatically generated when starting the client
  
+ Server files are located in the folder `./server_files` which should be in the same folder of TCPServer.jar
  + `credentials.txt` **MUST** be in the root directory of the server files. This file contains the users, accounts and passwords. Example is shown below.
  + The folders for each user/account will be automatically generated when you login to them if they do not exist. For example, user admin and acct admin1 the folder path will be `./server_files/admin/admin1/` This is the root directory for admin/admin1. They will not be able to go out of this folder so they do not touch important files on the server such as the credentials or system files.

### Adding additional users or accounts

1. Open `./server_files/credentials.txt`
2. Make a new line and write the user, account and password, all of which are TAB separated.
    + Note that an account **MUST** have a user

+ Note that there are three accounts already created:

```text
#user   #acct   #pass
admin   admin1  password123
guest
user1   test    surface
```

## Notes

+ Maximum file size that can be transferred is 2GB as the maximum array size for a byte[] is Integer.MAX_VALUE which is 2^31
+ Tested on Windows, Mac OS and Linux (Ubuntu 16.04.2 LTS)
+ Implemented using TCP
+ Sometimes verbose formatting doesn't format properly if the terminal's width is too short so you should extend it a little bit
+ Default type is Binary Mode
+ **File names cannot have spaces in them**

## Commands

For testing clarification:

+ `>>` indicates user input
+ `<<` indicates output
+ `!` means logged in
+ `+` means success
+ `-` means error
  
### USER `<user_id>`

This command sets the client's user to a valid user defined in the `credentials.txt` file. If there are no users in the file, it will use the client's input to create a new user and automatically log that user in. This is the first command that must be used correctly before proceeding. If the user is a public user, it will automatically log you in, otherwise you will need an account and password.
> Test Case

```console
>> USER guest
<< !guest logged in
>> USER admin
<< +admin valid, send account and password
>> USER
<< -Invalid arguments. Usage: USER <user_id>
>> USER randomUser
<< -Invalid randomUser, try again
>> USER so many arguments
>> -Invalid arguments. Usage: USER <user_id>
```

+ Guest is a public user and does not need to login
+ Admin is a private user with accounts and passwords for verification purposes. We see that the server responds correctly, prompting the user to send an account and password.
+ Specifying a user that is not registered with the server does not work and the server responds accordingly.
+ If you input no arguments or too many arguments, the server handles it and tells you how to use it.

### ACCT `<account>`

This command specify's the user's account to login with. The account and user is defined in `credentials.txt`. Must specify account if told to after specifying user.
> Test Case

```console
>> USER guest
<< !guest logged in
>> ACCT blah
<< !Already logged-in
>> USER admin
<< +admin valid, send account and password
>> ACCT admin
<< -Invalid account, try again
>> ACCT admin1
<< +Account valid, send password
>> ACCT
<< -Invalid arguments. Usage: ACCT <account>
>> ACCT many arguments
<< -Invalid arguments. Usage: ACCT <account>
```

+ Since the user guest has already been logged in, specifying account returns `!Already logged-in`
+ Switching user to admin requires the user to send account then password. Using an acct not registered to the user admin replies with `-Invalid account, try again` as expected.
+ Using a registered account the server replies with `+Account valid, send password` as expected
+ Using too many or too little arguments will throw an error

### PASS `<password>`

This command checks the user's password against the account located in `credentials.txt`. Must specify after being told to send password after specifying user and account. If the password matches, you will be logged in.
> Test Case

```console
>> USER admin
<< +admin valid, send account and password
>> ACCT admin1
<< +Account valid, send password
>> PASS rand#
<< -Wrong password, try again
>> PASS
<< -Invalid arguments. Usage: PASS <password>
>> PASS 1 2 3
<< -Invalid arguments. Usage: PASS <password>
>> PASS password123
<< !Logged in
>> PASS passAgain?
<< !Already logged-in
```

+ Using a password that does not match returns `-Wrong password, try again`
+ Having too little or too many arguments throws an error message
+ Using the correct password logs you in as seen by the message `!Logged in`
+ Trying to put another password in after being logged in tells you `!Already logged-in` as expected.

### TYPE `<A|B|C>`

This command changes the client's receiving type, either to type ASCII (A), Binary (B) or Continuous (C). ASCII mode means the client can only receive ASCII files. Binary or Continuous means send data in 8-bit bytes which is what most operating systems use so Binary and Continuous are the same. If a client tries to retrieve a binary file in ASCII mode, the server will reject the client's request. The default type is binary. You can only use this command once logged in.
> Test Case

```console
>> USER guest
<< !guest logged in
>> TYPE A
<< +Using ASCII mode
>> TYPE B
<< +Using Binary mode
>> TYPE C
<< +Using Continuous mode
>> TYPE D
<< -Type not valid
>> TYPE
<< -Invalid arguments. Usage: TYPE <A|B|C>
>> TYPE A B C
<< -Invalid arguments. Usage: TYPE <A|B|C>
```

+ Using TYPE <A|B|C> changes to that respective mode
+ Using a different type or argument the server says `-Type not valid`
+ Using too little or too many arguments throws an error
  
### LIST `<F|V> <directory (opt)>`

This command lists the files and folders in the specified directory or the current directory if no directory is specified. It should return a `+` followed by the current directory or directory specified and then its files and folders. F indicates standard formatting and will only list the file and folder names. V indicates verbose formatting and will list the file and folder names, creation times, last modified times, size, type (whether it's a directory or a file) and its permissions. Permissions include `r` for read, `w` for write and `x` for executable and `-` otherwise. This command can only be used once logged in.

+ NOTE: You should extend the terminal's width a little for verbose formatting to show properly.

> Test Case

```console
>> USER guest
<< !guest logged in
>> LIST F
<< +
<< /
<< test/
<< test.txt
<< wallpaper.jpg
>> LIST V
<< +
<< /
<< #Filename:      #Creation Time:      #Last Modified:      #Size:     #Type:     #Permissions:
<< test/           19/08/2018 20:45     21/08/2018 03:21     0B         Directory  rwx
<< test.txt        21/08/2018 00:52     21/08/2018 03:22     32B        File       rwx
<< wallpaper.jpg   21/08/2018 00:55     19/07/2012 21:56     1.24MB     File       rwx
>> LIST F test/
<< +
<< /test/
<< ./
<< ../
<< image.jpg
<< secret/
<< test2.txt
>> LIST V test/
<< +
<< /test/
<< #Filename:   #Creation Time:      #Last Modified:      #Size:     #Type:     #Permissions:
<< ./
<< ../
<< image.jpg    21/08/2018 03:21     26/12/2013 23:35     1.79MB     File       rwx
<< secret/      21/08/2018 03:49     21/08/2018 03:49     0B         Directory  rwx
<< test2.txt    19/08/2018 22:28     19/08/2018 22:48     27B        File       rwx
>> LIST F randDir/
<< -Invalid directory randDir
>> LIST F wallpaper.jpg
<< -Not a directory
>> LIST F a b c
<< -Invalid arguments. Usage: LIST <F|V> <directory (optional)>
>> LIST
<< -Invalid arguments. Usage: LIST <F|V> <directory (optional)>
>> LIST A
<< -Invalid format. Use F|V
```

+ Both formats F and V work as expected, with a specified directory and without one.
+ Not specifying a directory lists the contents of the current directory which is the root of guest in this case which can be found in `./server_files/guest/`
+ When specifying a directory, it lists the contents of that directory. This can be checked in `./server_files/guest/test/`
+ When trying to list a file or folder that doesn't exist, it will tell you it is an invalid directory.
+ When trying to list a file instead of a directory, it tells you it is not a directory.
+ Specifying too many or too little arguments will throw an error message
+ Using an incorrect format will also throw an error message
+ Note that the permissions and times may differ depending on the system that the server is hosted on
+ The size of the files are formatted in its SI form

### CDIR `<directory>`

This command changes the directory of where the user is in right now on the server, relative to the account it is on. Using `CDIR ..` will go back a folder (up till the root folder of the user or account). Using `CDIR /path/to/folder/` will change directory to that folder starting from the root folder or using `CDIR relative/path/` will navigate to that directory relative to the current directory. It can only be used when the user is logged in.
> Test Case

```console
>> USER guest
<< !guest logged in
>> CDIR
<< -Invalid arguments. Usage: CDIR <directory>
>> CDIR too many arguments
<< -Invalid arguments. Usage: CDIR <directory>
>> CDIR test/
<< !Changed working dir to /test/
>> CDIR secret/
<< !Changed working dir to /test/secret/
>> CDIR ..
<< !Changed working dir to /test/
>> CDIR test/secret/
<< -Cannot change directory because this directory does not exist
>> CDIR /test/secret/
<< !Changed working dir to /test/secret/
>> CDIR /
<< !Changed working dir to /
>> CDIR C:/Windows
<< -Cannot change directory because the directory does not exist
>> CDIR wallpaper.jpg
<< -Cannot change directory because the file specified is not a directory
```

+ If too little or too many arguments given, server throws an error
+ Going down one folder at a time is fine
+ Going back a folder using `..` is fine
+ Using relative path to `test/secret/` from the `test/` folder does not work as expected as there is no `test/test/secret` folder whereas using `/test/secret` is fine as it starts from the root.
+ Trying to `CDIR` to a file or folder that does not exist on the user's folder throws an error
+ Trying to `CDIR` to a file instead of a folder throws an error
+ You can check if the server is actually changing directory by following up the change in directories with a `LIST F` or `LIST V` command

### KILL `<file_name>`

This command deletes either a file specified or a folder and all its contents inside the folder. The files specified must be in the current directory, so you can only delete files you own. This command can only be used once logged in.
> Test Case
>> NOTE: Make a backup of your files before you delete them as they are PERMANENTLY deleted

```console
>> USER guest
<< !guest logged in
>> KILL wallpaper.jpg
<< +wallpaper.jpg deleted
>> KILL randfile
<< -Not deleted because the file does not exist
>> KILL
<< -Invalid arguments. Usage: KILL <filename>
>> KILL this and that
<< -Invalid arguments. Usage; KILL <filename>
>> KILL test/
<< +test deleted
```

+ Specifying a file deletes the file and specifying a folder deletes the folder and all of its contents
+ Specifying a random file that does not exist throws an error message
+ Specifying too little or too many arguments throws an error message
+ Please restore the files that you should have backed up so further test cases work correctly
+ You can see in the folder `./server_files/guest/` that the files you killed are gone

### NAME `<old_file_name>`

This command renames a file or folder specified. It gets the server ready to rename it to a new name. To complete the renaming, you must use the `TOBE <new_file_name>` command. This command can only be used once logged in.
> Test Case

```console
>> USER guest
<< !guest logged in
>> NAME test.txt
<< +File exists
>> TOBE OR NOT TO BE?
<< -Invalid arguments. Usage: TOBE <new_filename>
>> KILL test.txt
<< -Waiting for TOBE <new-file-name>
>> TOBE
<< -Invalid arguments. Usage: TOBE <new_filename>
>> TOBE new_test.txt
<< +test.txt renamed to new_test.txt
>> NAME test.txt
<< -Can't find test.txt
>> NAME test
<< +File exists
>> TOBE test2
<< +test renamed to test2
>> NAME test2
<< +File exists
>> TOBE new_test.txt
<< -File wasn't renamed because new_test.txt already exists
>> TOBE test
<< +test2 renamed to test
>> NAME
<< -Invalid arguments. Usage: NAME <filename>
>> NAME A NAME
<< -Invalid arguments. Usage: NAME <filename>
>> NAME new_test.txt
<< +File exists
>> TOBE test.txt
<< +new_test.txt renamed to test.txt
```

+ Too little or too many arguments throws an error in both `NAME` and `TOBE`
+ Trying to use other commands in the renaming phase is rejected as seen when trying to `KILL test.txt`
+ Using `NAME` on a file that does not exist throws an error that it can't find the file
+ After renaming test.txt and trying to rename it again, it didn't exist, shows that it worked.
+ Trying to rename `test2` to `new_test.txt` didn't work because it already existed

### RETR `<file_name>`

This command retrieves a file from the server to the client if it doesn't already have a file with that name. This command can only be used when logged in.
> Test Case
>> NOTE: The `SEND` and `STOP` commands are sent automatically to the server depending on the usable space the user has on their partition.

```console
>> USER guest
<< !guest logged in
>> RETR wallpaper.jpg
<< 1296749
<< SEND
<< +File wallpaper.jpg received
>> RETR wallpaper.jpg
<< 1296749
<< -Error file already exists, please rename it
<< STOP
<< +ok, RETR aborted
>> RETR randFile
<< -File doesn't exist
>> RETR
<< -Invalid arguments. Usage: RETR <filename>
>> RETR A B C
<< -Invalid arguments. Usage: RETR <filename>
>> TYPE A
<< +Using ASCII mode
>> RETR wallpaper.jpg
<< -Error not asn ASCII file, change to TYPE B|C
>> RETR test.txt
<< 32
<< SEND
<< +File test.txt received
```

+ The files will be stored in `./client_files/`
+ If the file transfer was not instant, i.e. transferring 1GB the line where it says `+File <filename> received` would display `Received ###.#%` Where ###.# is a number between 0.0% and 100.0% where the string keeps getting updated as bytes are being received (on the client).
+ Specifying too little or too many arguments throws an error
+ Specifying an invalid file throws an error
+ Trying to retrieve a binary file in ASCII mode doesn't work
+ Retrieving an ASCII file in ASCII mode works
+ Trying to retrieve a file you have with the same name throws an error

### STOR `<new|old|app> <file_name>`

This command stores a file from the client to the server. `NEW` means to create a file if it doesn't exist on the server's current directory or generate a new file if it already exists. `OLD` means to overwrite the file if it does exist, otherwise create it. `APP` means append to the file if it exists otherwise create a new file. Type checking does not apply here. It is sent as 8-bit bytes by default. This command can only be used when logged in.
> Test Case
>> Having done the RETR test case (or copying over test.txt and wallpaper.jpg to `./client_files/`)...

```console
>> USER guest
<< !guest logged in
>> KILL test.txt
<< +test.txt deleted
>> KILL wallpaper.jpg
<< +wallpaper.jpg deleted
>> STOR NEW test.txt
<< +File does not exist, will create new file
<< SIZE 32
<< +Saved test.txt
>> KILL test.txt
<< +test.txt deleted
>> STOR APP test.txt
<< +Will create file
<< SIZE 32
<< +Saved test.txt
>> STOR OLD wallpaper.jpg
<< +Will create new file
<< SIZE 1296749
<< +Saved wallpaper.jpg
>> STOR OLD wallpaper.jpg
<< +Will write over old file
<< SIZE 1296749
<< +Saved wallpaper.jpg
>> STOR NEW wallpaper.jpg
<< +File exists, will create new generation of file
<< SIZE 1296749
<< +Saved wallpaper(1).jpg
>> STOR APP test.txt
<< +Will append to file
<< SIZE 32
<< +Saved test.txt
>> STOR
<< -Invalid arguments. Usage: STOR <NEW|OLD|APP> <filename>
>> STOR A B C D
<< -Invalid arguments. Usage: STOR <NEW|OLD|APP> <filename>
>> STOR NEW virus
<< +File does not exist, will create new file
<< SIZE 0
<< Error unable to read file
<< -Couldn't save because file has no data
<< -Error invalid command
>> STOR OLD virus
<< +Will create new file
<< SIZE 0
<< Error unable to read file
<< -Couldn't save because file has no data
<< -Error invalid command
>> STOR APP virus
<< +Will create file
<< SIZE 0
<< Error unable to read file
<< -Couldn't save because file has no data
<< -Error invalid command
```

+ Note that the `SIZE` command is sent automatically and 0 is sent if the file is invalid from which the server throws an error.
+ Note that on the SERVER-SIDE it also shows that the file is received and the percentage is has received so far while transferring is incomplete (otherwise it just says `+File: <file-name> received`).
+ Inspecting the files, you can see that the text file has been appended twice.
+ A new wallpaper was created with the same name and an extra (1). Note that this will keep on working but will be very slow once it gets to high numbers since it iterates from 1 and keeps checking if the file exists.
+ Using `NEW`, `OLD` and `APP` to create new files works when they do not exist
+ Using `OLD` to overwrite an existing file does overwrite it. You can check this by using `STOR OLD test.txt` as it will overwrite the appended test.txt on the server. (Notice the difference).
+ Using too little or too many arguments throws an error

### DONE

This command signals the server that the client is done with the server and to close connections. This can be used whenever.
> Test Case

```console
>> DONE extra arguments
<< -Error invalid command
>> DONE
<< +127.0.0.1 closing connection
```

+ It ends the client connection with server
+ 127.0.0.1 will be replaced with the IP or hostname of the client.
+ To close the server you need to terminate it because it is always listening for new clients and has no stop command. Either use CTRL+C or COMMAND+C or force quit the server's terminal.

## Future Work

+ Use databases for storing users, accounts and passwords
+ Have one folder for server with certain folders being locked to certain accounts rather than having to create folders for each user and account.
+ Allow users to press the `TAB` key to autocomplete their words if possible
+ Allow transfer of files over 2GB, possibly split into parts
+ Better verbose formatting (text wrapping / resizing depending on size of terminal)
+ Use checksums to check if file transferred was not corrupted
+ `HELP` function which lists all valid commands and their descriptions

## Authors thoughts about specification

+ `TYPE` is unnecessary
+ `USER` is unnecessary
+ Should input user before password
+ Some commands can be shortened i.e. `LIST` = `ls` `CDIR` = `cd`
+ `TOBE` is unnecessary could just do `NAME <old_file_name> <new_file_name>`
