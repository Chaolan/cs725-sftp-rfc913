package SFTP.client;

import java.io.*;
import java.net.*;
import java.nio.file.Files;

public class TCPClient {

    private static Socket clientSocket;
    private static BufferedReader serverInput;
    private static DataOutputStream outToServer;

    public static void main(String args[]) {

        if (args.length != 2) {
            System.err.println("Usage: java KKMultiServer <host name> <port number>");
            System.exit(1);
        }

        // Create client files directory for client if it does not exist
        File directory = new File("./client_files");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String inputLine;
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(System.in));
        int portNumber = Integer.parseInt(args[1]);

        String hostName = args[0];

        //Setup
        try {
            clientSocket = new Socket(hostName, portNumber);
            serverInput = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            outToServer = new DataOutputStream(clientSocket.getOutputStream());

            try {
                //Read welcome message
                System.out.println(readMessage());
                //Reading and writing to server
                while ((inputLine = inFromClient.readLine()) != null) {
                    outToServer.writeBytes(inputLine + "\0");
                    String serverLine = readMessage();
                    System.out.println(serverLine);

                    //Handle retrieval of file from server
                    if (serverLine.matches("[0-9]+")) {
                        //If the server sends back numbers it means it sent back the size of file to be received
                        handleRETR(inputLine, serverLine);
                    }

                    // All these messages means the server is ready to store from the client
                    if ((serverLine.equals("+File exists, will create new generation of file")) ||
                            (serverLine.equals("+File does not exist, will create new file")) ||
                            (serverLine.equals("+Will write over old file")) ||
                            (serverLine.equals("+Will create new file")) ||
                            (serverLine.equals("+Will append to file")) ||
                            (serverLine.equals("+Will create file"))) {
                        handleSTOR(inputLine);
                    }

                    //server has closed connection with client so client closes connection too
                    if (serverLine.endsWith("closing connection")) {
                        break;
                    }
                }
            } catch (SocketException e) {
                System.out.println("-SERVER DISCONNECTED");
            }
            clientSocket.close();
        } catch (ConnectException e) {
            System.out.println("-UNABLE TO CONNECT TO SERVER " + hostName + ":" + portNumber);
            System.exit(1);
        } catch (IOException e) {
            System.out.println("-UNABLE TO READ/WRITE");
            System.exit(1);
        }
    }

    //Reads message from server until a null char '\0' is met then returns the message
    private static String readMessage() {
        Character c;
        StringBuilder message = new StringBuilder();
        try {
            while ((c = (char) serverInput.read()) != '\0') {
                message.append(c);
            }
        } catch (IOException e) {
            System.out.println("-UNABLE TO READ FROM SERVER");
        }
        return message.toString();
    }

    //Receives the file from the server and stores it in ./client_files/filename
    private static void handleRETR(String inputLine, String serverLine) {
        int sizeInBytes = Integer.parseInt(serverLine);
        String fileName = inputLine.split(" ")[1]; // RETR <filename>
        String[] split = fileName.split("/"); //In case retr specified with i.e. /setup/rfc913.pdf
        fileName = split[split.length-1];
        File file = new File("./client_files/" + fileName);
        try {
            if (file.createNewFile()) {
                if (file.getUsableSpace() > sizeInBytes) {
                    outToServer.writeBytes("SEND\0"); //Ready to receive on client
                    System.out.println("SEND");
                    byte[] bytes = new byte[sizeInBytes];
                    InputStream is = clientSocket.getInputStream();
                    int bytesReceived = 0;
                    while((bytesReceived += is.read(bytes, bytesReceived, bytes.length-bytesReceived)) != bytes.length) {
                        //Reads bytes being sent by the server until it's received all bytes specified
                        String percentage = String.format("%.1f",(float)bytesReceived/sizeInBytes*100);
                        //Dynamically print percentage received
                        System.out.print("\rReceived " + percentage + "%");
                        System.out.flush();
                    }
                    Files.write(file.toPath(), bytes);
                    System.out.println("\r+File " + file.getName() + " received");
                    //Confirmation to user that file has been retrieved from server
                } else {
                    outToServer.writeBytes("STOP\0");
                    System.out.println("-Error not enough space");
                    System.out.println("STOP");
                    System.out.println(readMessage()); //Read the +ok, RETR stopped message
                }
            } else {
                outToServer.writeBytes("STOP\0");
                System.out.println("-Error file already exists, please rename it");
                System.out.println("STOP");
                System.out.println(readMessage()); //Read the +ok, RETR stopped message
            }
        } catch (IOException e) {
            //Unable to connect to the server's input/output
            System.out.println("-Error unable to read/write file");
            //Disconnect from the server
            try {
                clientSocket.close();
                System.out.println("-Disconnected from server");
                System.exit(1);
            } catch (IOException e2) {
                System.out.println("-Unable to close connection with server");
                System.out.println("-Terminating client");
                System.exit(1);
            }
        }
    }

    //Writes data to the server
    private static void handleSTOR(String input) {
        String[] splitInput = input.split(" ");
        String fileName = splitInput[2]; //STOR <CMD> FILENAME
        File file = new File("./client_files/" + fileName);
        try {
            outToServer.writeBytes("SIZE " + file.length() + "\0");
            System.out.println("SIZE " + file.length());
            String serverLine = readMessage(); //Read the +ok message
            if (serverLine.equals("+ok, waiting for file")) {
                //Send file
                byte[] bytes = Files.readAllBytes(file.toPath());
                outToServer.write(bytes);
                System.out.println(readMessage()); //Read the +File saved message
            }
        } catch (IOException e) {
            System.out.println("Error unable to read file");
            try {
                outToServer.writeBytes("SIZE 0\0"); //Invalid file
                System.out.println(readMessage()); //To get +ok, message
                System.out.println(readMessage()); //To get -Couldn't save because...
            } catch (IOException e2) {
                System.out.println("Unable to write to server");
            }
        }
    }

}


