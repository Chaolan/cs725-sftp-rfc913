package SFTP.server;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;

public class TCPServerHandler {

    private String HostName;
    private boolean isDone;
    private String user;
    private String acct;
    private boolean loggedIn;
    private String baseDir; // ./server_files/user/acct
    private String cDir; // /..
    private Character type; // A | B | C
    private Boolean toBe; // File getting renamed
    private String oldName; // Old file name
    private Boolean readyToSend; // Ready to send file to client
    private String fileToSend;
    private Boolean readyToRecv; // Ready to receive file from client
    private String fileToRecv;
    private Boolean appendFile; // Append file being received to existing file or not
    private int recvFileSize;

    public TCPServerHandler(String name) {
        HostName = name;
        isDone = false;
        user = null;
        acct = null;
        loggedIn = false;
        baseDir = "";
        cDir = "/"; // default directory is /
        toBe = false;
        oldName = null;
        type = 'B'; // default transfer type is Binary
        readyToSend = false;
        fileToSend = "";
        readyToRecv = false;
        fileToRecv = "";
        appendFile = false;
        recvFileSize = 0;
    }

    public String processInput(String input) {
        String upperInput = input.split(" ")[0].toUpperCase(); //To ignore cases... "CMD args.."

        if (toBe) { //File to be renamed, wait for TOBE command
            if (upperInput.equals("TOBE")) {
                return handleToBe(input);
            } else {
                return "-Waiting for TOBE <new-file-name>";
            }
        }

        if (readyToSend) { //File to be sent to client
            return handleSend(upperInput);
        }

        if (readyToRecv) { //File to be received from client
            if (upperInput.equals("SIZE")) {
                return handleReceive(input);
            } else {
                return "-Error, please send \"SIZE <number-of-bytes-in-file>\"";
            }
        }

        if (upperInput.equals("USER")) {
            return handleUser(input);
        } else if (upperInput.equals("ACCT")) {
            return handleAcct(input);
        } else if (upperInput.equals("PASS")) {
            return handlePass(input);
        } else if (input.equalsIgnoreCase("DONE")) {
            return handleDone();
        } else if (loggedIn) {
            switch (upperInput) {
                case "TYPE": return handleType(input);
                case "LIST": return handleList(input);
                case "KILL": return handleKill(input);
                case "NAME": return handleName(input);
                case "RETR": return handleRETR(input);
                case "STOR": return handleSTOR(input);
                case "CDIR": return handleCDIR(input);
            }
        } else if (!loggedIn) {
            return "-Please login first";
        }
        return "-Error invalid command";
    }

    //Gets buffered reader for a text file in ./server_files/
    private BufferedReader getReader(String fileName) {
        try {
            File file = new File ("./server_files/" + fileName);
            InputStream is = new FileInputStream(file);
            InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return new BufferedReader(streamReader);
        } catch (FileNotFoundException e) {
            System.out.println("ERROR: File not found");
        }
        return null;
    }

    //Gets print writer for a text file in ./server_files/
    private PrintWriter getWriter(String fileName) {
        try {
            File file = new File ("./server_files/" + fileName);
            OutputStream os = new FileOutputStream(file);
            return new PrintWriter(os, true);
        } catch (FileNotFoundException e) {
            System.out.println("ERROR: File not found");
        }
        return null;
    }

    // Generates directory for logged in user if it does not exist
    private void generateDirectories() {
        if (this.user != null && this.acct != null) {
            File directory = new File("./server_files/"+this.user+"/"+this.acct);
            if (!directory.exists()) {
                directory.mkdirs();
            }
        } else if (this.user != null) {
            File directory = new File("./server_files/"+this.user);
            if (!directory.exists()) {
                directory.mkdir();
            }
        }
    }

    /*
    Sets the user to user specified if valid and logs them in if the user is a superuser.
    When changing user, the user must login again to change accounts.
     */
    private String handleUser(String input) {
        //Check credentials again if changing user.
        loggedIn = false;
        this.user = null;
        this.acct = null;

        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String user = splitInput[1];
            BufferedReader reader = getReader("credentials.txt");
            try {
                reader.readLine(); //read top line detailing #user  #acct   #pass
                //If file is empty create a new user
                if (!reader.ready()) {
                    PrintWriter writer = getWriter("credentials.txt");
                    writer.println(user);
                    this.user = user.toLowerCase();
                    loggedIn = true;
                    baseDir = "./server_files/" + this.user;
                    generateDirectories();
                    return "!" + user + " logged in!";
                } else {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        String[] splitLine = line.split("\t");
                        if (user.equalsIgnoreCase(splitLine[0])) { //spitLine[0] is the user in credentials.txt
                            if (splitLine.length == 1) { //No acct or password i.e. superuser
                                loggedIn = true;
                                this.user = user.toLowerCase();
                                baseDir = "./server_files/" + this.user;
                                generateDirectories();
                                return "!" + user + " logged in";
                            } else {
                                this.user = user.toLowerCase();
                                return "+" + user + " valid, send account and password";
                            }
                        }
                    }
                    this.user = null;
                    return "-Invalid " + user + ", try again";
                }
            } catch (IOException e) {
                System.out.println("ERROR: Could not read file");
            }
        }
        return "-Invalid arguments. Usage: USER <user_id>";
    }

    //Sets the user's account if it is linked to the "user" and is valid.
    private String handleAcct(String input) {

        if (loggedIn) {
            return "!Already logged-in";
        }

        if (this.user == null) {
            return "-Send user first"; //Specify user first
        }

        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String acct = splitInput[1];
            BufferedReader reader = getReader("credentials.txt");
            try {
                String line;
                while((line = reader.readLine()) != null) {
                    String[] splitLine = line.split("\t"); //0 = user, 1 = acct, 2 = password
                    if (splitLine.length == 3) {
                        if (acct.equalsIgnoreCase(splitLine[1])) {
                            if (this.user.equalsIgnoreCase(splitLine[0])) {
                                this.acct = acct.toLowerCase();
                                return "+Account valid, send password";
                            }
                        }
                    }
                }
                return "-Invalid account, try again";
            } catch (IOException e) {
                System.out.println("ERROR: Unable to read file");
            }
        }
        return "-Invalid arguments. Usage: ACCT <account>";
    }

    //Checks the password provided and if it matches against the account registered in the handler in credentials.txt
    private String handlePass(String input) {

        if (loggedIn) {
            return "!Already logged-in";
        }

        if (this.acct == null) {
            return "-Send account first";
        }

        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String pass = splitInput[1];
            BufferedReader reader = getReader("credentials.txt");
            try {
                String line;
                while((line = reader.readLine()) != null) {
                    String[] splitLine = line.split("\t"); //0 = user, 1 = acct, 2 = password
                    if (splitLine.length == 3) {
                        if (this.user.equalsIgnoreCase(splitLine[0]) && this.acct.equalsIgnoreCase(splitLine[1])
                                && pass.equals(splitLine[2])) {
                            loggedIn = true;
                            baseDir = "./server_files/" + this.user + "/" + this.acct;
                            generateDirectories();
                            return "!Logged in";
                        }
                    }
                }
                return "-Wrong password, try again";
            } catch (IOException e) {
                System.out.println("ERROR: Unable to read file");
            }
        }
        return "-Invalid arguments. Usage: PASS <password>";
    }

    //Changes file type to either A, B or C.
    private String handleType(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String type = splitInput[1];
            if (type.equalsIgnoreCase("A")) {
                this.type = 'A';
                return "+Using ASCII mode";
            } else if (type.equalsIgnoreCase("B")) {
                this.type = 'B';
                return "+Using Binary mode";
            } else if (type.equalsIgnoreCase("C")) {
                this.type = 'C';
                return "+Using Continuous mode";
            } else {
                return "-Type not valid";
            }
        }
        return "-Invalid arguments. Usage: TYPE <A|B|C>";
    }

    //Returns a list of files and directories at the specified directory, dirPath
    private String getContents(String dirPath) {
        //dirPath unspecified then dir is just baseDir + cDir, otherwise add dirPath to it.
        String dir = (dirPath == null) ? baseDir + cDir : baseDir + cDir + dirPath;
        File f = new File(dir);
        if (f.exists()) {
            if (f.isDirectory()) {
                File[] files = f.listFiles();
                StringBuilder output = new StringBuilder();
                output.append("+\r\n");
                //dirPath unspecified then current directory is just cDir else add dirPath to it
                output.append((dirPath == null) ? cDir + "\r\n" : cDir + dirPath + "\r\n");
                //Not root directory then add ./ and ../ to the next two lines
                if (!dir.equals(baseDir+"/")) {
                    output.append("./\r\n");
                    output.append("../\r\n");
                }

                for (File file : files) {
                    output.append(file.getName());
                    if (file.isDirectory()) {
                        output.append("/");
                    }
                    output.append("\r\n");
                }
                output.delete(output.length() - 2, output.length()); //Removes the extra \r\n at the end
                return output.toString();
            } else {
                return "-Not a directory";
            }
        } else {
            return "-Invalid directory " + f.getName();
        }
    }

    //Converts a number of bytes into its SI form
    private String convertToSI(long bytes) {
        String size;
        if (bytes >= 1024) {
            int exp = (int) (Math.log(bytes) / Math.log(1024)); //Exponent for 1024
            char pre = "kMGTPE".charAt(exp-1); //i.e. exp = 1 then prefix is k for Kilo, 2 for Mega, 3 for Giga, etc.
            size = String.format("%.2f%cB", bytes / Math.pow(1024,exp), pre); // Format string to X.XXZB where Z is the prefix
        } else {
            size = Long.toString(bytes) + "B"; //It's just XB where X is the number of bytes.
        }
        return size;
    }

    //Returns the size of the longest file name given an array of files
    private int longestFileName(File[] files) {
        if (files == null) {
            return 0;
        }
        int longestName = 0;
        for(File f : files) {
            if (f.getName().length() > longestName) {
                longestName = f.getName().length();
            }
        }
        return longestName;
    }

    /*
    Returns a formatted list of files and directories in the format of:
    #Filename   #Creation Time  #Last Modified  #Size   #Type   #Permissions
    Times are in the format dd/MM/yyyy HH:mm (i.e. 19/08/2018 21:29)
    Size is in its SI units i.e. 1.3GB or 1MB rather than 130000000000 Bytes
    Type is either File or Directory
    Permissions: r means read, w means write, x means executable
    i.e. rwx means readable, writable and executable; r-- means readable; -w- means writable; --x means executable
     */
    private String getVerbose(String dirPath) {
        //dirPath unspecified then dir is just baseDir + cDir, otherwise add dirPath to it.
        String dir = (dirPath == null) ? baseDir + cDir : baseDir + cDir + dirPath;
        File f = new File(dir);
        if(f.exists()) {
            if (f.isDirectory()) {
                File[] files = f.listFiles();
                StringBuilder output = new StringBuilder();
                output.append("+\r\n");
                //dirPath unspecified then current directory is just cDir else add dirPath to it
                output.append(dirPath == null ? cDir + "\r\n" : cDir + dirPath + "\r\n");
                int maxNameLength = longestFileName(files) + 2; //+2 for extra spacing
                if (maxNameLength < 12) { //Allocate space for #Filename:
                    maxNameLength = 12;
                }
                output.append(String.format("#%-"+maxNameLength+"s", "Filename:")); //# left adjust, 60 spaces
                output.append(String.format("#%-20s", "Creation Time:"));
                output.append(String.format("#%-20s", "Last Modified:"));
                output.append(String.format("#%-10s", "Size:"));
                output.append(String.format("#%-10s", "Type:"));
                output.append(String.format("#%-15s", "Permissions:"));
                output.append("\r\n");
                //Not root directory then add ./ and ../ to the next two lines
                if (!dir.equals(baseDir+"/")) {
                    output.append("./\r\n");
                    output.append("../\r\n");
                }

                for (File file : files) {
                    Path path = file.toPath();
                    try {
                        BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                        String fileName = file.getName();
                        if (file.isDirectory()) {
                            fileName += "/";
                        }
                        String creationTime = df.format(attr.creationTime().toMillis());
                        String lastModifiedTime = df.format(attr.lastModifiedTime().toMillis());
                        long bytes = attr.size();
                        String size = convertToSI(bytes);

                        output.append(String.format("%-"+maxNameLength+"s",fileName));
                        output.append(String.format(" %-20s",creationTime));
                        output.append(String.format(" %-20s",lastModifiedTime));
                        output.append(String.format(" %-10s", size));

                        if (file.isDirectory()) {
                            output.append(String.format(" %-10s", "Directory"));
                        } else if (file.isFile()) {
                            output.append(String.format(" %-10s", "File"));
                        }

                        StringBuilder permissions = new StringBuilder();
                        permissions.append(file.canRead() ? "r" : "-");
                        permissions.append(file.canWrite() ? "w" : "-");
                        permissions.append(file.canExecute() ? "x" : "-");

                        output.append(String.format(" %-15s", permissions.toString()));
                        output.append("\r\n");
                    } catch (IOException e) {
                        System.out.println("Error, no attributes found");
                    }
                }
                output.delete(output.length()-2, output.length()); //Deleting the extra \r\n
                return output.toString();
            } else {
                return "-Not a directory";
            }
        }
        return "-Invalid directory " + f.getName();
    }

    //Calls getContents or getVerbose depending on the format specified, F or V.
    private String listContents(String format, String dirPath) {
        if (format.equalsIgnoreCase("F")) {
            return getContents(dirPath);
        } else if (format.equalsIgnoreCase("V")) {
            return getVerbose(dirPath);
        }
        return "-Invalid format. Use F|V";
    }

    //Calls list contents which returns the contents of directory specified formatted by type F or V.
    private String handleList(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 3) { // Directory specified
            String format = splitInput[1];
            String dirPath = splitInput[2];
            return listContents(format, dirPath);
        } else if (splitInput.length == 2) { // No directory specified (current directory)
            String format = splitInput[1];
            return listContents(format, null);
        }
        return "-Invalid arguments. Usage: LIST <F|V> <directory (optional)>";
    }

    //Changes the current directory for the user or account specified by the input
    private String handleCDIR(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) { //CDIR <dir>
            String dir = splitInput[1];
            dir = dir.replace("\\", "/"); // Windows uses \ for file separators, change it to / for uniformity.
            String newDir;
            if (dir.equals("..")) { //Go back one directory
                if (!cDir.equals("/")) { //Current directory not root then split CDIR and add all except the last directory to go back
                    String[] dirs = cDir.split("/");
                    StringBuilder backDir = new StringBuilder();
                    for (int i = 0; i < dirs.length - 1; i++) {
                        backDir.append(dirs[i]);
                        backDir.append("/");
                    }

                    if (dirs.length == 0) { //If for some reason the current directory is for example "///" this will be true
                        newDir = "/"; //Just make the new directory the root.
                    } else {
                        newDir = backDir.toString();
                    }
                } else { //Current directory is the root so going back should do nothing
                    newDir = "/";
                }
            } else if (dir.startsWith("/")) { //Navigating from root directory

                //Go back to root directory or new directory i.e. /movies/downloads
                if(dir.endsWith("/")) {
                    newDir = dir;
                } else {
                    newDir = dir + "/"; //adds an extra "/" if it wasn't specified
                }

                //If for some reason the directory was "//////////" it'll set it to "/"
                if (dir.matches("[/]*")) {
                    newDir = "/";
                }

            } else if (dir.startsWith(".")) { // Ignore directories starting with .
                newDir = cDir; //Don't change directories
            } else { //Folder specified i.e. movies or movies/
                if (dir.endsWith("/")) {
                    newDir = cDir + dir;
                } else {
                    newDir = cDir + dir + "/"; //Add extra "/" if wasn't specified
                }
            }

            File f = new File(baseDir + newDir);
            if (f.exists()) {
                if (f.isDirectory()) {
                    cDir = newDir;
                    return "!Changed working dir to " + cDir;
                } else {
                    return "-Cannot change directory because the file specified is not a directory";
                }
            } else {
                return "-Cannot change directory because the directory does not exist";
            }
        } else {
            return "-Invalid arguments. Usage: CDIR <directory>";
        }
    }

    //Recursively deletes files inside a folder or just deletes a file if a file is specified.
    private String deleteRecursive(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    deleteRecursive(f);
                }
            }
        }

        if (file.delete()) {
            return "+" + file.getName() + " deleted";
        } else {
            return "-Not deleted because unable to delete " + file.getName();
        }
    }

    //Deletes a the file specified in the current directory
    private String handleKill(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String fileName = splitInput[1];
            File file = new File(baseDir + cDir + fileName);
            if (file.exists()) {
                return deleteRecursive(file);
            } else {
                return "-Not deleted because the file does not exist";
            }
        } else {
            return "-Invalid arguments. Usage: KILL <filename>";
        }
    }

    //Gets ready to rename a file if it exists
    private String handleName(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String oldName = splitInput[1];
            File file = new File(baseDir + cDir + oldName);
            if (file.exists()) {
                toBe = true;
                this.oldName = oldName;
                return "+File exists";
            } else {
                return "-Can't find " + oldName;
            }
        } else {
            return "-Invalid arguments. Usage: NAME <filename>";
        }
    }

    //Renames old file name to new file name
    private String handleToBe(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) {
            String newName = splitInput[1];
            File oldFile = new File(baseDir + cDir + oldName);
            File newFile = new File(baseDir + cDir + newName);
            if (newFile.exists()) {
                return "-File wasn't renamed because " + newName + " already exists";
            } else {
                if (oldFile.renameTo(newFile)) {
                    String temp = this.oldName;
                    this.oldName = null;
                    toBe = false;
                    return "+" + temp + " renamed to " + newName;
                }
            }
        }
        return "-Invalid arguments. Usage: TOBE <new_filename>";
    }

    //Checks whether a file is a binary file or not
    private int isBinary(File file) {
        try {
            byte[] bytes = Files.readAllBytes(file.toPath());
            for (byte b : bytes) {
                if (b < 0) { //Java uses signed bits so if the MSB is 1 it means it's negative
                    // If the MSB is 1 that means it's a binary file, not an ASCII file.
                    return 1;
                }
            }
            return 0; //is ASCII
        } catch (IOException e) {
            System.out.println("Unable to read file");
            return -1;
        }
    }

    //Closes connection with the client
    private String handleDone() {
        String output = "+"+HostName+" closing connection";
        isDone = true;
        return output;
    }

    //Checks whether file to be sent to client has correct type, i.e. Type A = ASCII mode, Type B = binary mode
    private String handleRETR(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) { // RETR <filename>
            String fileName = splitInput[1];
            File file = new File(baseDir + cDir + fileName);
            if (file.exists()) {
                if (file.isFile()) {
                    if (type == 'A') {
                        if (isBinary(file) != 0) {
                            return "-Error not an ASCII file, change to TYPE B|C";
                        }
                    }
                    //File exists
                    this.fileToSend = fileName;
                    this.readyToSend = true;
                    return Long.toString(file.length()); //Size of file
                } else {
                    return "-Error not a file";
                }
            } else {
                return "-File doesn't exist";
            }
        }
        return "-Invalid arguments. Usage: RETR <filename>";
    }

    //Generates new name for a file for example newFile(1).txt
    private String generateNewName(String fileName) {
        File file = new File(baseDir + cDir + fileName);
        int number = 1;

        String[] splitName = fileName.split("\\."); //Split file's name and it's extension

        if (splitName.length == 1) {
            //File name does not have a file extension
            while(file.exists()) {
                String newName = fileName + "(" + number + ")";
                file = new File(baseDir + cDir + newName);
                number++;
            }
        } else {
            while (file.exists()) {
                StringBuilder newName = new StringBuilder();
                for (int i = 0; i < splitName.length; i++) {
                    if (i == splitName.length - 2) { //String just before the file type
                        newName.append(splitName[i]);
                        newName.append("(");
                        newName.append(number);
                        newName.append(")."); // "." for the file extension coming up next
                    } else {
                        newName.append(splitName[i]);
                        newName.append(".");
                    }
                }
                newName.deleteCharAt(newName.length()-1); // Delete the extra dot added to the end.
                file = new File(baseDir + cDir + newName.toString());
                number++;
            }
        }
        return file.getName();
    }

    /*
    Handles STOR NEW command which either creates a file if it doesn't exist or if it does exist it creates the file
    again with a (number) next to it starting from 1.
     */
    private String handleNEW(String fileName) {
        File file = new File(baseDir + cDir + fileName);
        if (file.exists()) { //Create new file with number next to it i.e. (1)
            String newName = generateNewName(fileName);
            file = new File(baseDir + cDir + newName);
            try {
                if (file.createNewFile()) {
                    readyToRecv = true;
                    fileToRecv = newName;
                    return "+File exists, will create new generation of file";
                } else {
                    return "-Unable to create file";
                }
            } catch (IOException e) {
                return "-Unable to create file";
            }
        } else { //File doesn't already exist so just makes a new one
            try {
                if (file.createNewFile()) {
                    readyToRecv = true;
                    fileToRecv = fileName;
                    return "+File does not exist, will create new file";
                } else {
                    return "-Unable to create file";
                }
            } catch (IOException e) {
                return "-Unable to create file";
            }
        }
    }

    //Overwrites the old file specified by deleting it and making a new one or creating a new one if it doesn't exist
    private String handleOLD(String fileName) {
        readyToRecv = true;
        fileToRecv = fileName;
        File file = new File(baseDir + cDir + fileName);
        if (file.exists()) {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                return "-Unable to create file";
            }
            return "+Will write over old file";
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                return "-Unable to create file";
            }
            return "+Will create new file";
        }
    }

    //Creates a new file if it doesn't exist or tells the system to append to the file if it exists
    private String handleAPP(String fileName) {
        readyToRecv = true;
        fileToRecv = fileName;
        File file = new File(baseDir + cDir + fileName);
        if (file.exists()) {
            appendFile = true;
            return "+Will append to file";
        } else {
            try {
                file.createNewFile();
                return "+Will create file";
            } catch (IOException e) {
                return "-Unable to create file";
            }
        }
    }

    //Calls handleNEW / handleOLD / handleAPP to get ready to receive files
    private String handleSTOR(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 3) { // STOR <command> <filename>
            String cmd = splitInput[1];
            String fileName = splitInput[2];
            if (cmd.equalsIgnoreCase("NEW")) {
               return handleNEW(fileName);
            } else if (cmd.equalsIgnoreCase("OLD")) {
                return handleOLD(fileName);
            } else if (cmd.equalsIgnoreCase("APP")) {
                return handleAPP(fileName);
            } else {
                return "-Error invalid command please send \"STOR <NEW|OLD|APP> filename\"";
            }
        }
        return "-Invalid arguments. Usage: STOR <NEW|OLD|APP> <filename>";
    }

    //Handles the SIZE <number-of-bytes> command sent by client
    private String handleReceive(String input) {
        String[] splitInput = input.split(" ");
        if (splitInput.length == 2) { //Argument checking "SIZE <number-of-bytes>"
            if (splitInput[1].matches("[0-9]+")) { //Check if argument is a number
                recvFileSize = Integer.parseInt(splitInput[1]);
                File file = new File(baseDir + cDir + fileToRecv);
                this.readyToRecv = false;
                if (file.getUsableSpace() > recvFileSize) { //Checking free space
                    return "+ok, waiting for file";
                } else { //No space to store file stop receiving
                    this.appendFile = false; // Reset append file
                    return "-Not enough room, don't send it";
                }
            } else {
                //Argument specified wasn't a number
                return "-Invalid argument. Usage: SIZE <number-of-bytes-in-file>";
            }
        } else {
            //Too many or too little arguments specified
            return "-Invalid arguments. Usage: SIZE <number-of-bytes-in-file>";
        }
    }

    //Receives the file from the client
    public String receiveFile(InputStream fileStream) {
        File file = new File(baseDir + cDir + fileToRecv);
        if (recvFileSize <= 0) {  // Size is either 0 or negative means the file doesn't exist on client side
            if(file.exists()) {
                if(!appendFile) {
                    file.delete();
                    //Only delete it if it wasn't appending to it. If it was appending to it, then the file SHOULD exist on the server side.
                }
            }
            return "-Couldn't save because file has no data";
        }

        try {
            byte[] bytes = new byte[recvFileSize];
            int bytesReceived = 0;
            while((bytesReceived += fileStream.read(bytes, bytesReceived, recvFileSize-bytesReceived)) != recvFileSize) {
                //Keep reading from the inputstream until bytesReceived is equal to the file size to be received
                //Show percentage of file being stored on server terminal
                String percentage = String.format("%.1f", (float)bytesReceived/recvFileSize*100);
                System.out.print("\rReceived " + percentage + "%"); //Dynamic change in percentage displayed on server side
                System.out.flush();
            }
            //Finished receiving data from client
            if (appendFile) {
                appendFile = false;
                Files.write(file.toPath(), bytes, StandardOpenOption.APPEND); //Append to the file
            } else {
                Files.write(file.toPath(), bytes); //Write data to the file
            }
            System.out.println("\r+File: " + file.getName() + " received ");
            return "+Saved " + file.getName();
        } catch (IOException e) {
            System.out.println("-Unable to store file from client");
        }
        return "-Couldn't save because unable to read from client";
    }

    //Handles the SEND or STOP command sent by client
    private String handleSend(String input) {
        if (input.equals("SEND")) {
            this.readyToSend = false;
            return baseDir + cDir + fileToSend; // File path to be sent over to server thread
        } else if (input.equals("STOP")) {
            this.readyToSend = false;
            return "+ok, RETR aborted";
        } else {
            return "-Invalid command. Please use SEND or STOP";
        }
    }

    //Sends file to send to the output stream (client)
    public void sendFile(DataOutputStream out) {
        File file = new File(baseDir + cDir + fileToSend);
        try {
            byte[] bytes = Files.readAllBytes(file.toPath());
            out.write(bytes);
        } catch (IOException e) {
            System.out.println("-Unable to send file to client");
        }
    }

    //Returns true is the client is done with the server
    public boolean isDone() {
        return isDone;
    }

}
