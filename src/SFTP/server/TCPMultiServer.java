package SFTP.server;

import java.io.*;
import java.net.*;

public class TCPMultiServer {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Usage: java KKMultiServer <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            System.out.println("+Server started on port " + portNumber);
            while (true) {
                new TCPServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("-Could not listen on port " + portNumber);
            System.exit(-1);
        }
    }

}
