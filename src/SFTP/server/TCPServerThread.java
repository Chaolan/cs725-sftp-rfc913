package SFTP.server;

import java.net.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

public class TCPServerThread extends Thread {
    private Socket clientSocket;

    public TCPServerThread(Socket socket) {
        super("TCPServerThread");
        clientSocket = socket;
    }

    public void run() {
        String hostName = clientSocket.getInetAddress().getHostName();
        String inputLine, outputLine;
        try {
            //Setup
            TCPServerHandler handler = new TCPServerHandler(hostName);
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

            //Welcome message
            out.writeBytes("+" + hostName + " SFTP Service\0");

            // Reading and writing from/to client
            while (true) {
                inputLine = readMessage(in);
                outputLine = handler.processInput(inputLine);

                if (inputLine.equalsIgnoreCase("SEND") && outputLine.startsWith("./server_files/")) {
                    //output line returns directory if it is OKAY to send, otherwise it'll send an error message
                    handler.sendFile(out);
                } else if (outputLine.equals("+ok, waiting for file")) {
                    out.writeBytes(outputLine + "\0"); //send confirmation to client
                    InputStream is = clientSocket.getInputStream();
                    outputLine = handler.receiveFile(is); //receive file from client
                    out.writeBytes(outputLine + "\0"); //send file received / not received to client
                } else {
                    out.writeBytes(outputLine + "\0"); // send every other message to the client
                }

                if (handler.isDone()) { //if the client is done with server it'll break and close the connection and thread
                    break;
                }
            }
            clientSocket.close();
        } catch (IOException e) {
            System.out.println("Unable to read/write to client");
        }
    }

    //Reads message from server until a null char '\0' is met then returns the message
    private String readMessage(BufferedReader in) {
        Character c;
        StringBuilder message = new StringBuilder();
        try {
            while ((c = (char) in.read()) != '\0') {
                message.append(c);
            }
        } catch (IOException e) {
            System.out.println("-Unable to read from client");
        }
        return message.toString();
    }

}
